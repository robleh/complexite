package Activite3;

/**
 *
 * @author robleh
 */
public class Lettre extends Courrier{
   private String format; 
   private Boolean estRecommande;

    public Lettre(Double poid, String adresse, String code) {
        super(poid, adresse, code);
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Boolean getEstRecommande() {
        return estRecommande;
    }

    public void setEstRecommande(Boolean estRecommande) {
        this.estRecommande = estRecommande;
    }
   
}
