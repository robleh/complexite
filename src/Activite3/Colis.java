package Activite3;

/**
 *
 * @author robleh
 */
public class Colis extends Courrier {

    private double litre;
    private boolean esRecommande;

    public Colis(Double poid, String adresse, String code) {
        super(poid, adresse, code);
    }

    public double getLitre() {
        return litre;
    }

    public void setLitre(double litre) {
        this.litre = litre;
    }

    public boolean isEsRecommande() {
        return esRecommande;
    }

    public void setEsRecommande(boolean esRecommande) {
        this.esRecommande = esRecommande;
    }

    
}
