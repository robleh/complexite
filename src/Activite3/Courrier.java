package Activite3;

/**
 *
 * @author robleh
 */
public class Courrier {

    private Double poid;
    private String adresse;
    private String code;
    private Timber timber[] = new Timber[20];

    public Courrier(Double poid, String adresse, String code) {
        this.poid = poid;
        this.adresse = adresse;
        this.code = code;
    }

    private boolean timbervide() {
        for (Timber timber1 : timber) {
            if (timber1 != null) {
                return false;
            }
        }
        return true;
    }

    /*
    la méthode estValide qui 
    indique si un courrier possède une
    adresse non vide et au moins un timbre 
     */
    public boolean estVide() {
        if (this.adresse != null && timbervide() != true) {
            return true;
        }
        return false;
    }

    /*
    Une méthode calculerTotalTimbres 
    qui renvoie la valeur totale des
    timbres placés sur le courrier 
     */
    public float calculerTotalTimbres() {
        float tt = 0;
        for (Timber timber1 : this.timber) {
            if (timber1 != null) {
                tt = tt + Integer.parseInt(timber1.getValeur());
            }
        }
        return tt;
    }

    /*
    la méthode calculerTaxeEnvoi qui renvoie 
    le montant de la taxe devant être payée pour 
    un courrier non recommandé en se basant sur
    la tableau 
     */
    public float calculerTaxeEnvoi() {
        return 0;
    }

    /*
    Une méthode calculerTarifEnvoi qui renvoie 
    le tarif de l’envoi d’un courrier en prenant 
    compte du fait qu’il est recommandé ou pas. 
     */
    public float calculerTarifEnvoi() {
        return 0;
    }

    /*
    Une méthode estExpediable qui renvoie
    si un courrier peut être expédié ou non. 
    Un courrier peut être expédié 
    s’il est valide et si la valeur des 
    timbres placés couvre (dépasse) la 
    valeur du tarif d’envoi. 
     */
    public boolean estExpediable() {
        //if(estVide()!=false && )
        return false;
    }

    @Override
    public String toString() {
        return getPoid() + " " + getAdresse() + " " + getCode() + " " + calculerTotalTimbres(); //To change body of generated methods, choose Tools | Templates.
    }

    /*
    Une méthode ajouterTimbre permettant d’ajouter un
    timbre au courrier et renvoie si l’opération est
    possible
     */
    boolean ajouterTimbre(Timber t) {
        for (Timber timber1 : this.timber) {
            if (timber1 != null) {
                timber1 = t;
                return true;
            }
        }
        return false;
    }

    public Double getPoid() {
        return poid;
    }

    public void setPoid(Double poid) {
        this.poid = poid;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Timber[] getTimber() {
        return timber;
    }

    public void setTimber(Timber[] timber) {
        this.timber = timber;
    }

}

//class A {
//
//    public static int f(int x) {
//        return (x + 5);
//    }
//
//    ; 
//public int g(int x) {
//        return (3);
//    }
//}
//
//class D extends A {
//
//    public static int f(int x) {
//        return (x + 4);
//    }
//
//    ;
//public int g(int x) {
//        return (x + 8);
//    }
//}
//
//public class Courrier {
//
//    public static void main(String args[]) {
//       String s1="bonjour";
//
//String s2="bonjour";
//
//String s3= new String ("bonjour");
//
//System.out.println(s1==s2);
//
//System.out.println(s3==s2);
//
//System.out.println(s1.equals(s3));
//
//    }
//}
