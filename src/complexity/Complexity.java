package complexity;

/**
 * @author robleh
 */
public class Complexity {

    public static void main(String[] args) {
        System.out.println("run");
        int n = 634;
        int n1 = croisant(n);
        System.out.println(croisant(213));
        int n2 = decroisant(n);
        int k = n2 - n1;
        soustration(k);
    }

    static int soustration(int n) {
        int k = 0;
        while (true) {
            k = n;
            n = decroisant(n) - croisant(n);
            if (k == n) {
                System.out.println(n);
                break;
            }
        }
        return n;
    }

    static int croisant(int n) {
        boolean rang = false;
        int n3 = n % 10;
        int n2 = (n % 100) / 10;
        int n1 = (n % 1000) / 100;
        int tab[] = new int[3];
        tab[0] = n1;
        tab[1] = n2;
        tab[2] = n3;
        System.out.println(n1 + " " + n2 + " " + n3);
        if (tab.length <= 3) { //
            try {
                int i = 0;
                while (rang != true) {
                    if (i + 1 < tab.length) {
                        int r1 = tab[i];
                        int r2 = tab[i + 1];
                        if (r1 < r2 && r1 > 0 && r2 > 0) {
                            tab[i] = r1;
                            tab[i + 1] = r2;
                        } else {
                            tab[i] = r2;
                            tab[i + 1] = r1;
                        }
                    }
                    rang = order(tab);
                    i++;
                    if (i >= tab.length) {
                        i = 0;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int result = tab[0] * 100 + tab[1] * 10 + tab[2];
        return result;
    }

    static boolean order(int[] tab) {
        boolean cond = false;
        for (int i = 0; i < tab.length; i++) {
            if (i + 1 < tab.length) {
                if (tab[i] <= tab[i + 1]) {
                    cond = true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        return cond;
    }

    static int decroisant(int n) {
        boolean rang = false;
        int n3 = n % 10;
        int n2 = (n % 100) / 10;
        int n1 = (n % 1000) / 100;
        int tab[] = new int[3];
        tab[0] = n1;
        tab[1] = n2;
        tab[2] = n3;
        if (tab.length <= 3) {
            try {
                int i = 0;
                while (rang != true) {
                    if (i + 1 < tab.length) {
                        int r1 = tab[i];
                        int r2 = tab[i + 1];
                        if (r1 > r2 && r1 > 0 && r2 > 0) {
                            tab[i] = r1;
                            tab[i + 1] = r2;
                        } else {
                            tab[i] = r2;
                            tab[i + 1] = r1;
                        }
                    }
                    rang = dorder(tab);
                    i++;
                    if (i >= tab.length) {
                        i = 0;
                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        int result = tab[0] * 100 + tab[1] * 10 + tab[2];
        return result;
    }

    static boolean dorder(int[] tab) {
        boolean cond = false;
        for (int i = 0; i < tab.length; i++) {
            if (i + 1 < tab.length) {
                if (tab[i] >= tab[i + 1]) {
                    cond = true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        return cond;
    }

}
